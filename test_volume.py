import unittest
import volume


class TestVolume(unittest.TestCase):

    def test_combination_negative(self):
        self.assertEqual(volume.volume_cube(1, 2, 3), 6)
        self.assertEqual(volume.volume_cube(-1, 2, 3), -6)
        self.assertEqual(volume.volume_cube(-1, -2, 3), 6)
        self.assertEqual(volume.volume_cube(-1, -2, -3), -6)
        self.assertEqual(volume.volume_cube(1, -2, -3), 6)
        self.assertEqual(volume.volume_cube(1, 2, -3), -6)

    def test_zero_dimensions(self):
        self.assertEqual(volume.volume_cube(0, 0, 0), 0)
        self.assertEqual(volume.volume_cube(0, 0, 5), 0)
        self.assertEqual(volume.volume_cube(0, 3, 0), 0)
        self.assertEqual(volume.volume_cube(4, 0, 0), 0)

    def test_float_dimensions(self):
        self.assertEqual(volume.volume_cube(1.5, 2.5, 3.5), 13.125)
        self.assertEqual(volume.volume_cube(1.2, 2.7, 3.9), 12.636000000000001)

    def test_large_dimensions(self):
        self.assertEqual(volume.volume_cube(1000, 1000, 1000), 1000000000)
        self.assertEqual(volume.volume_cube(99999, 99999, 99999), 999970000299999)

    def test_invalid_input(self):
        string = "123 can't have egg, bacon, sausage, and spam"
        self.assertRaises(ValueError, volume.volume_cube, string, 2, 3)
        with self.assertRaises(ValueError):
            volume.volume_cube('a', 2, 3)
        with self.assertRaises(ValueError):
            volume.volume_cube(1, 'b', 3)
        with self.assertRaises(ValueError):
            volume.volume_cube(1, 2, 'c')

    def test_negative_dimensions(self):
        self.assertEqual(volume.volume_cube(-999, -999, -999), -997002999)
        self.assertEqual(volume.volume_cube(-9999, -9999, -9999), -999700029999)


if __name__ == '__main__':
    unittest.main()
